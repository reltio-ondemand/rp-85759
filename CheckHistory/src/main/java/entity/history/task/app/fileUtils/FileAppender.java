package entity.history.task.app.fileUtils;

import java.io.*;
import java.util.Collection;
import java.util.function.Consumer;

public class FileAppender implements Consumer<Collection<String>> {

    String filePath;
    private BufferedWriter bufferedWriter;
    boolean successfullyInit;

    public FileAppender(String filePath) {
        this(filePath, false);
    }

    public FileAppender(String filePath, boolean append) {
        this.filePath = filePath;
        this.successfullyInit = false;
        init(append);
    }

    private void init(boolean append) {
        try {
            bufferedWriter = new BufferedWriter(new FileWriter(new File(filePath), append), 512);
            successfullyInit = true;
        } catch (IOException e) {
            e.printStackTrace();
            successfullyInit = false;
        }
    }

    @Override
    public void accept(Collection<String> data) {
        if (successfullyInit) {
            try {
                for (String item : data) {
                    bufferedWriter.write(item + "\n");

                }
                bufferedWriter.flush();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    public void finalHook() {
        if (bufferedWriter != null) {
            try {
                bufferedWriter.flush();
                bufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

}
