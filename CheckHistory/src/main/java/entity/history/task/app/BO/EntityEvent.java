package entity.history.task.app.BO;

import java.util.Objects;

public class EntityEvent {


    String eventType;
    String user;
    Long time;

    public EntityEvent(String eventType, String user, Long time) {
        this.eventType = eventType;
        this.user = user;
        this.time = time;
    }

    public String getEventType() {
        return eventType;
    }

    public String getUser() {
        return user;
    }

    public Long getTime() {
        return time;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof EntityEvent)) return false;
        EntityEvent that = (EntityEvent) o;
        return Objects.equals(getEventType(), that.getEventType()) &&
                Objects.equals(getUser(), that.getUser()) &&
                Objects.equals(getTime(), that.getTime());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getEventType(), getUser(), getTime());
    }
}
