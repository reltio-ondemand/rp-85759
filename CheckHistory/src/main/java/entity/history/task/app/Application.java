package entity.history.task.app;

import entity.history.task.app.fileUtils.FileAppender;
import entity.history.task.app.fileUtils.InputDataProvider;
import entity.history.task.app.fileUtils.http.AuthClient;
import entity.history.task.app.fileUtils.http.RestApiClient;
import org.apache.log4j.LogManager;

import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicLong;

public class Application {

    private static final org.apache.log4j.Logger logger = LogManager.getLogger(Application.class);

    private static Properties prop = new Properties();
    private static final long START_TIME = System.currentTimeMillis();

    private static class BatchWorker implements Runnable {

        private final RestApiClient restApiClient;
        private final AtomicLong requestCount;
        FileAppender eventsToCreateAppender;
        List<String> toProcess;
        private AtomicLong updateEntityCount;

        FileAppender progressAppender;

        BatchWorker(
                RestApiClient restApiClient,
                List<String> toProcess,
                AtomicLong updateEntityCount,
                AtomicLong requestCount, FileAppender eventsToCreateAppender, FileAppender progressAppender) {
            this.restApiClient = restApiClient;
            this.toProcess = new ArrayList<>(toProcess);
            this.requestCount = requestCount;
            this.updateEntityCount = updateEntityCount;
            this.eventsToCreateAppender = eventsToCreateAppender;
            this.progressAppender = progressAppender;
        }

        @Override
        public void run() {
            for (String entityID : toProcess) {
                try {
                    requestCount.incrementAndGet();
                    int size = restApiClient.getEntity(entityID,updateEntityCount, eventsToCreateAppender, progressAppender);
                    updateEntityCount.addAndGet(size);

                } catch (Exception e) {
                    progressAppender.accept(Collections.singletonList(entityID +",  "+ e.getMessage()));
                    logger.error(e);
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {

        if (args.length == 0) {
            System.out.println("Input Properties not specified");
            return;
        }
        String file = args[0];

        prop.load(new FileReader(file));

        System.out.format("USER=%s \n," +
                        " PASSWORD=%s \n" +
                        " INPUT_FILE=%s \n" +
                        " AUTH_URL=%s \n" +
                        " TENANT=%s \n" +
                        " SERVER_URI=%s \n",
                prop.getProperty("USER"),
                prop.getProperty("PASSWORD"),
                prop.getProperty("INPUT_FILE"),
                prop.getProperty("AUTH_URL"),
                prop.getProperty("TENANT"),
                prop.getProperty("SERVER_URI")
        );
        AuthClient authClient = new AuthClient(
                prop.getProperty("AUTH_URL"),
                prop.getProperty("USER"),
                prop.getProperty("PASSWORD"));

        RestApiClient restApiClient = new RestApiClient(
                prop.getProperty("SERVER_URI"),
                prop.getProperty("TENANT"),
                authClient
        );
        long ts = System.currentTimeMillis();
        FileAppender eventsToCreateAppender = new FileAppender("./entityEventsToCreate_" + ts + ".csv", true);
        FileAppender progressAppender = new FileAppender("./data_" + ts + ".csv", true);

        InputDataProvider inputDataProvider = new InputDataProvider(prop.getProperty("INPUT_FILE"));
        Iterator<String> stringIterator = inputDataProvider.getStringIterator();

        AtomicLong updateEntityCount = new AtomicLong(0);
        AtomicLong requestCount = new AtomicLong(0);
        // write headers to output files

        List<String> buffer = new ArrayList<>();
        ExecutorService executor = Executors.newFixedThreadPool(50);
        while (stringIterator.hasNext()) {
            String inputString = stringIterator.next();
            buffer.add(inputString);
            if (buffer.size() >= 50) {
                executor.submit(new BatchWorker(restApiClient, buffer,
                        updateEntityCount, requestCount, eventsToCreateAppender, progressAppender));
                buffer.clear();

            }
        }
        if (!buffer.isEmpty()) {
            executor.submit(new BatchWorker(restApiClient, buffer,
                    updateEntityCount, requestCount, eventsToCreateAppender, progressAppender));
        }

        executor.shutdown();
        while (!executor.isTerminated()) {
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        progressAppender.accept(Collections.singletonList("Finished.took total time=" + (System.currentTimeMillis() - START_TIME) / (1000.0 * 60)));
        progressAppender.accept(Collections.singletonList(String.format("recordCount=%s, entityUpdateCunt=%s",
                requestCount, updateEntityCount)));
        progressAppender.finalHook();
        eventsToCreateAppender.finalHook();
        logger.info("Finished.took total time=" + (System.currentTimeMillis() - START_TIME) / (1000.0 * 60));
        logger.info("Finished");
    }
}

