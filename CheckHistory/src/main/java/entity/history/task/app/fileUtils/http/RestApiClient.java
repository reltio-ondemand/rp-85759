package entity.history.task.app.fileUtils.http;

import entity.history.task.app.fileUtils.FileAppender;
import org.apache.commons.io.IOUtils;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.log4j.LogManager;
import org.json.JSONArray;
import org.json.JSONObject;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.concurrent.atomic.AtomicLong;


public class RestApiClient {

    private static final org.apache.log4j.Logger logger = LogManager.getLogger(RestApiClient.class);

    private final String serverUrl;
    private final String tenant;
    private final AuthClient authClient;

    public RestApiClient(String serverUrl, String tenant, AuthClient authClient) {
        this.serverUrl = serverUrl;
        this.tenant = tenant;
        this.authClient = authClient;
    }

    /**
     * this method fetches entity and for every Specialities attribute wuth ignored=true
     * sends request to unignore attribute
     *  @param entityId          : entityID id of entity
     * @param updateEntityCount running count of entities that needed update.
     * @param eventLog          output file to record events details
     * @return
     */

    public int getEntity(String entityId, AtomicLong updateEntityCount, FileAppender eventLog, FileAppender progressAppender) {
        Collection<String> events = new ArrayList<>();
        try {
            progressAppender.accept(Collections.singletonList(entityId));
            logger.info("entityId=" + entityId);
            HttpGet getEntity = new HttpGet(String.format("%s/api/%s/entities/%s?select=createdTime,updatedTime,type,uri,createdBy,updatedBy",
                    serverUrl,
                    tenant,
                    entityId));
            getEntity.setHeader("Authorization", String.format("Bearer %s", authClient.getAccessToken()));
            getEntity.setHeader("Content-Type", "application/json");
            CloseableHttpClient httpClient = HttpClients.createDefault();
            CloseableHttpResponse response = httpClient.execute(getEntity);
            if (response.getStatusLine().getStatusCode() >= 400) {
                throw new Exception(String.format("Entity %s getting error: %s", entityId, response.getStatusLine().getStatusCode()));
            }
            String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
            JSONObject jsonObject = new JSONObject(responseContent);
            String uri = jsonObject.getString("uri");
            String type = jsonObject.getString("type");
            String createdBy = jsonObject.getString("createdBy");
            String updatedBy = jsonObject.getString("updatedBy");
            long createdTime = jsonObject.getLong("createdTime");
            long updatedTime = jsonObject.getLong("updatedTime");

            events = getHistory(uri, createdTime, createdBy, updatedTime, updatedBy, type);
            if (!events.isEmpty()) {
                System.out.println(updateEntityCount);
                updateEntityCount.incrementAndGet();
                eventLog.accept(events);
            }else {

                System.out.println("ignore,"+ entityId);
            }
        } catch (Exception ex) {
            logger.error(ex);
        }
        return events.size() > 0 ? 1 : 0;

    }

    private Collection<String> getHistory(String uri, long createdAt, String createdBy, long updatedAt,
                                          String updatedBy, String type) throws Exception {
        Collection<String> events = new ArrayList<>();
        HttpGet getEntity = new HttpGet(String.format("%s/api/%s/%s/_changes?",
                serverUrl,
                tenant,
                uri));
        getEntity.setHeader("Authorization", String.format("Bearer %s", authClient.getAccessToken()));
        getEntity.setHeader("Content-Type", "application/json");
        CloseableHttpClient httpClient = HttpClients.createDefault();
        CloseableHttpResponse response = httpClient.execute(getEntity);
        if (response.getStatusLine().getStatusCode() != 200) {
            throw new Exception(String.format("Entity %s getting error: %s", uri,
                    response.getStatusLine().getStatusCode()));
        }

        String responseContent = IOUtils.toString(response.getEntity().getContent(), StandardCharsets.UTF_8.name());
        JSONArray array = new JSONArray(responseContent);

        final String createEvent = String.format("%s, %s, %s, %s, %s", uri.substring(uri.indexOf("/") + 1),
                type.substring(type.lastIndexOf("/") + 1),
                "ENTITY_CREATED",
                createdBy,
                createdAt);
        final String updateEvent = String.format("%s, %s, %s, %s, %s", uri.substring(uri.indexOf("/") + 1),
                type.substring(type.lastIndexOf("/") + 1),
                "ENTITY_CHANGED",
                updatedBy,
                updatedAt);

        if (array.length() == 0) {
            events.add(createEvent);
            if (createdAt < updatedAt) {
                events.add(updateEvent);
            }

        } else {
            long maxUpdated = 0;
            long maxRemoved = 0;
            boolean hasCreateEvent = false;
            for (int i = 0; i < array.length(); i++) {
                JSONObject jsonObject = array.getJSONObject(i);
                if (jsonObject.has("type") && jsonObject.getString("type").equals("ENTITY_CREATED")) {
                    hasCreateEvent = true;
                } else if (jsonObject.has("type") && jsonObject.getString("type").equals("ENTITY_CHANGED")) {
                    long ts = jsonObject.getLong("timestamp");
                    maxUpdated = Math.max(ts, maxUpdated);
                }else if(jsonObject.has("type")  && jsonObject.getString("type").equals("ENTITY_REMOVED")){
                    long ts = jsonObject.getLong("timestamp");
                    maxRemoved = Math.max(ts, maxRemoved);
                }
            }
            if (maxUpdated != 0 && maxUpdated < updatedAt) {
                events.add(updateEvent);
            }  if (maxRemoved != 0 &&  maxRemoved < updatedAt) {
                events.add(updateEvent.replace("ENTITY_CHANGED", "ENTITY_REMOVED"));
            }
            if (!hasCreateEvent) {
                events.add(createEvent);
            }
        }
        return events;
    }
}
